# ECS Docker CPU leak tester

This project is a simple docker container with the purpose of demonstrating and reproducing a CPU leak in dockerd process when using AWS ECS as the cluster manager.

To reproduce the CPU leak you simply have to run the docker container over and over multiple times, and slowly the minimum CPU usage of your instance will increase. If you ssh to the docker host instance, you will see that this CPU use is taken by the dockerd process.

# How to use

You will have to create your own ecs cluster and you only need 1 instance. The instance type used was t3.small using spot fleet (just to save cost). Some specific startup script (user data) was also used, at least to provide the cluster name, but also some tunings we do normally in our dataflows. At some point, some tests were also done without these tuning values, and the CPU leak was seen as well, but the exact startup script was like this:
 ```
 #!/bin/bash
cat <<'EOF' >> /etc/ecs/ecs.config
ECS_CLUSTER=dockercpuleak
ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION=1m
ECS_IMAGE_CLEANUP_INTERVAL=10m
EOF
```
You can change the cluster name to the name of your choice (or remove it for the default one) but will have to change the run-on-ecs.sh script accordingly.
The AMI used in the last test where the issue was reproduced was this one:
```
amzn2-ami-ecs-hvm-2.0.20181017-x86_64-ebs (ami-075d44ed0d20df780)
```
Althought the issue was also seen on amzn linux v1 AMIs as well as other releases of ECS AMIs release before.

The file build-docker.sh is self exaplanatory, it only build the docker image locally. You will still need to create your repo (like on ECR) and push your container image there.

There is a run.sh to run the docker one time, and run-local.sh to run multiple times locally, we could not reproduce the issue locally on a Ubuntu 16.04 machine with docker ce 18.09.1, but more tests may be needed to see the same problem.

run-on-ecs.sh is what was used to run on ECS. You will need to create your own task definition in ecs. Only the memory was set to 32mb and CPU wasn't specified, for the container specification, the "Auto-configure CloudWatch Logs" option was checked, although it may not be needed since the test application do not output any log.

After running the "run-on-ecs-sh" script for about an hour you should already see the minimum CPU usage increased when the host instance is supposed to be idle (should be clearly over 1%). After running it overnight, the minimum CPU usage reported in "top" on the host was between 12-15%, although the CloudWatch monitoring was reporting a lot more with a constant 40%+ CPU usage even when the instance was idle for a long time, CPU credit balance was at 0 and unlimitted t3 credits were being used. Restarting the dockerd daemon restores the cpu usage back to 0% (or close to). 

# Details of the test & dockerfile

The project is very simple, it was found originally that this leak was happening in all our pipelines and we were using mostly python. So this docker container is using python for that reason, althought it does not seem you need to run any code.

# Simplified test

The issue can also be reproduced simply by using the hello-world public docker image, no need to run any python application. You can simply edit the dockefile and change FROM python:3-slim to FROM hello-world and remove or comment out the rest of the dockerfile.

# Importance of this issue

If you are running a cluster where you have tasks starting several times per hour (in our case it was around 500 tasks per hour in a cluster of 40 t3.small instances), you will slowly but certainly see the average (and minimum) CPU usage increase over time. In our case it was around 1% increase per 4 days. Over time you will get over the 20% CPU average given by t3.small instance and will incur CPU credit usage.

If you are running ECS services with containers that are always up and running, you won't be really experiencing any noticeable cpu increase.

# Mitigations

You can reboot your instances every couple of days, depending on your usage, or restarting the dockerd daemon seems to work, althought the impact of that in an ECS cluster while it is running tasks is unknown. If you are using the AWS provided AMIs and you don't want to manage the host OS and settings, this is more of a problem, and in our case we haven't implemented any workaround so far.

If other mitigations or fixes are known or found they can be reported in the Issue tracker