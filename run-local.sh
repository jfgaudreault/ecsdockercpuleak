#!/bin/bash

while :
do

    p=4
    pids=[]

    trap clean SIGINT
    clean() {
        for i in `seq 0 $p`; do
            kill ${pids[i]} 2> /dev/null
        done
        exit
    }


    for i in `seq 1 $p`; do
        ./run.sh &
        pids[i]=$!
    done

    sleep 3

done


