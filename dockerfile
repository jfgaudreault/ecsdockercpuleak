FROM python:3-slim 

WORKDIR /app

ADD requirements.txt /app
RUN pip3 install -U pip && pip3 install --trusted-host pypi.python.org -r requirements.txt

ADD test.py /app

ENV NAME dockercpuleak

ENTRYPOINT ["python", "test.py"]
