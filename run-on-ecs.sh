#!/bin/bash

while :
do

    p=40
    pids=[]

    trap clean SIGINT
    clean() {
        for i in `seq 0 $p`; do
            kill ${pids[i]} 2> /dev/null
        done
        exit
    }


    for i in `seq 1 $p`; do
        aws ecs run-task --region us-east-2 --cluster dockercpuleak --task-definition dockercpuleak:1 &
        pids[i]=$!
    done

    sleep 3

done


